import { BrowserRouter as Router, Route } from 'react-router-dom'
import HomePage from '../../features/home/presentation/home_page'
import DataCardPage from '../../features/data-card/presentation/data_card_page'

function Routers() {
	return (
		<>
			<Router>
				<Route exact path='/'>
					<HomePage/>
				</Route>
				<Route path='/data-card/:name' component={DataCardPage} />
			</Router>
    	</>
	);
}

export default Routers;
