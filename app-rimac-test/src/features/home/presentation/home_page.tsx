import { Button, Checkbox, CheckboxProps, createStyles, FormControl, FormControlLabel, Grid, InputLabel, makeStyles, MenuItem, Select, TextField, Theme, withStyles } from "@material-ui/core";
import './home.sass'
import { useHistory } from "react-router-dom";
import { green } from '@material-ui/core/colors';
import React,{useEffect, useState } from "react";

const useStyles = makeStyles((theme: Theme) =>
createStyles({
    formControl: {
    	minWidth: 80,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
	}),
);


function HomePage(){
	let us ={
		title :'',
		first :'',
		last :'',
	}
	const [ use, setEquipo] = useState(null);

	useEffect(()=>{
		obtenerDatos()
	}, [])

	const obtenerDatos = async() =>{
		const data = await fetch('https://randomuser.me/api/', );
		const user = await data.json()
		console.log(user['results'][0]['name']);
		us = user['results'][0]['name'];
		setEquipo(null);
		// const users = await data.json(),
	}

	let history = useHistory();

	function handleClick() {
		history.push(`/data-card/${us.first}`,);
	}
	const GreenCheckbox = withStyles({
		root: {
			color: green[400],
			'&$checked': {
			color: green[600],
			},
		},
		checked: {},
	})((props: CheckboxProps) => <Checkbox color="default" {...props} />);
	const [state, setState] = React.useState({
		checkedA: true,
		checkedB: true,
		checkedF: true,
		checkedG: true,
	});
	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setState({ ...state, [event.target.name]: event.target.checked });
	};

	const [age, setAge] = React.useState('');

	const handleChanges = (event: React.ChangeEvent<{ value: unknown }>) => {
		setAge(event.target.value as string);
	};
	const classes = useStyles();

	return (
		<>
			<Grid container alignContent="flex-start"   style={{height:'100vh'}} className ='dad'>
				<Contact/>
				<Grid item xs={12} sm={4} className = 'banner' >
				</Grid>
				<Grid container  item  xs ={12} sm={7} justify="center" alignContent='center'  style={{height:'100vh'}}>
					<Grid container item xs={10} sm={4} >
						<h1>Déjanos tus datos</h1>
						<form action="">
							<Grid container>
								<Grid item xs={3}>
								<FormControl variant="outlined" className={classes.formControl}>
									<InputLabel id="demo-simple-select-outlined-label">Age</InputLabel>
									<Select
									style={{width:'100%'}}
									labelId="demo-simple-select-outlined-label"
									id="demo-simple-select-outlined"
									value={age}
									onChange={handleChanges}
									label="DNI"
									>
									<MenuItem value="">
										<em>None</em>
									</MenuItem>
									<MenuItem value={10}>DNI</MenuItem>
									<MenuItem value={30}>RUC</MenuItem>
									</Select>
								</FormControl>
								</Grid>
								<Grid item xs={9}>
									<TextField
										style={{width:'100%', marginBottom:'20px'}}
										id="outlined-helperText"
										label="Nro. de doc"
										variant="outlined"
									/>
								</Grid>
							</Grid>
							<TextField
								style={{width:'100%',marginBottom:'20px'}}
								id="celular"
								label="Celular"
								type="search"
								variant="outlined"
							/>
							<TextField
								style={{width:'100%', marginBottom:'20px'}}
								id="outlined-helperText"
								label="Placa"
								variant="outlined"
							/>
							<Grid container>
								<Grid item xs={1}>
									<FormControlLabel
										control={<GreenCheckbox checked={state.checkedG} onChange={handleChange} name="checkedG" />}
										label=""
									/>
								</Grid>
								<Grid item xs={11}>
									<p> Acepto la Politica de Proteccion de Datos Personales y los términos y Condiciones</p>
								</Grid>
							</Grid>
							<Button variant="contained" color="secondary" onClick={handleClick}>
								COTÍZALO
							</Button>
						</form>
					</Grid>
				</Grid>
			</Grid>
		</>
	);
}
export default HomePage;

function Contact(){
	return(
		<Grid container justify="space-around" item xs={12} className = 'child' >
			<Grid item xs={4} >
				<p>rimac</p>
			</Grid>
			<Grid container item xs={4} className='contact1' justify="flex-end">
				<p>?Tienes alguna duda? (01)411 6001</p>
			</Grid>
			<Grid container item xs={4} className='contact' justify="flex-end">
				<p>llámanos</p>
			</Grid>
		</Grid>
	);
}